public class Vacancy {
   private String name;

    Vacancy(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
