import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {
    public static String url = "https://hh.ru/search/vacancy?text=Программист&page=0";
    private final String USER_AGENT = "Mozilla/5.0";
    public static VacancyList vl = new VacancyList();
    DBController dbController = new DBController();
    static int countOfVacansies = 105;

    public static void main(String[] args) throws Exception {

        Main http = new Main();

        System.out.println("Testing 1 - Send Http GET request");
        http.getVacanciesFromRequest(http.sendGet(url), countOfVacansies);
        System.out.println("-------------------------------");

        int line = 1;
        for (Vacancy v:vl.vacancies) {
            System.out.print(line++ + "   ");
            System.out.println(v.getName());
        }
    }

    private void getVacanciesFromRequest(String htmlResponse, int coutOfVacancies) throws Exception {

        for (int i = 0; i < coutOfVacancies; i++) {
            String v;
            int start = htmlResponse.indexOf("vacancy-serp__vacancy-title");
            int end;

            //after reading all employes
            if (start == -1){
                int pageStart;
                int pageEnd;
                pageStart = htmlResponse.indexOf("pager-page\">");
                pageStart = htmlResponse.indexOf(">",pageStart) + 1;
                pageEnd = htmlResponse.indexOf("<", pageStart);
                int page = Integer.parseInt(htmlResponse.substring(pageStart,pageEnd));

                if (htmlResponse.indexOf("page=" + Integer.toString(page) + "\">") == -1){
                    break;
                }else {
                    url = url.substring(0,url.indexOf("page=") + 5) + page;
                    Main http = new Main();
                    coutOfVacancies -= i;
                    http.getVacanciesFromRequest(http.sendGet(url), coutOfVacancies);
                    break;
                }
            }

            start = htmlResponse.indexOf(">", start);
            end = htmlResponse.indexOf("<", start);
            v = htmlResponse.substring(start + 1, end);
                //check for unwanted characters
                if (v.indexOf("\'") != -1){
                   v = v.replace("\'","");
                }
            vl.addVacancy(v);
            dbController.addVacancyToDB(v);
            htmlResponse = htmlResponse.substring(end + 1);
        }
    }


    // HTTP GET request
    private String sendGet(String url) throws Exception {

//        String url = "https://hh.ru/search/vacancy?text=Программист";

        URL obj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }


}
